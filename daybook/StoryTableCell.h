//
//  StoryTableCell.h
//  daybook
//
//  Created by Development on 7/20/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIView *color;

@end

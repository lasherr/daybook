//
//  CDRRestClient.h
//  daybook
//
//  Created by Development on 7/26/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDRRestClient : NSObject

- (void) getNetworkData;

@end

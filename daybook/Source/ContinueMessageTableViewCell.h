//
//  ContinueMessageTableViewCell.h
//  daybook
//
//  Created by Development on 7/15/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *ContinueMessengerCellIdentifier = @"ContinueMessengerCell";

@interface ContinueMessageTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *bodyLabel;

@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic) BOOL usedForMessage;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

+ (CGFloat)defaultFontSize;

@end

//
//  ContinueMessageTableViewCell.m
//  daybook
//
//  Created by Development on 7/15/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "ContinueMessageTableViewCell.h"

#import "SLKUIConstants.h"

@implementation ContinueMessageTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        
        [self configureSubviews];
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGFloat pointSize = [ContinueMessageTableViewCell defaultFontSize];
    
    self.bodyLabel.font = [UIFont systemFontOfSize:pointSize];
    
    self.bodyLabel.text = @"body";
}

- (void)configureSubviews
{
    [self.contentView addSubview:self.bodyLabel];
    
    NSDictionary *views = @{
                            @"bodyLabel": self.bodyLabel,
                            };
    
    NSDictionary *metrics = @{@"tumbSize": @(0),
                              @"padding": @15,
                              @"right": @10,
                              @"left": @45
                              };
    
    [self.contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-left-[bodyLabel(>=0)]-right-|"
                                             options:0 metrics:metrics views:views]];
    
    if ([self.reuseIdentifier isEqualToString:ContinueMessengerCellIdentifier]) {
        [self.contentView addConstraints:[NSLayoutConstraint
                                          constraintsWithVisualFormat:@"V:|-right-[bodyLabel(>=0@999)]-right-|" options:0 metrics:metrics views:views]];
    }
    else {
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[bodyLabel]|" options:0 metrics:metrics views:views]];
    }
}

- (UILabel *)bodyLabel
{
    if (!_bodyLabel) {
        _bodyLabel = [UILabel new];
        _bodyLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _bodyLabel.backgroundColor = [UIColor clearColor];
        _bodyLabel.userInteractionEnabled = NO;
        _bodyLabel.numberOfLines = 0;
        _bodyLabel.textColor = [UIColor darkGrayColor];
        _bodyLabel.font = [UIFont systemFontOfSize:[ContinueMessageTableViewCell defaultFontSize]];
    }
    return _bodyLabel;
}

+ (CGFloat)defaultFontSize
{
    CGFloat pointSize = 16.0;
    
    NSString *contentSizeCategory = [[UIApplication sharedApplication] preferredContentSizeCategory];
    pointSize += SLKPointSizeDifferenceForCategory(contentSizeCategory);
    
    return pointSize;
}

@end

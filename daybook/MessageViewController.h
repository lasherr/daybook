//
//  MessageViewController.h
//  daybook
//
//  Created by Development on 7/14/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "Source/SLKTextViewController.h"

#import "IrcController.h"


@interface MessageViewController : SLKTextViewController

@property (nonatomic, strong) NSString *channel;
@property (nonatomic, strong) NSString *network;
//@property (nonatomic, strong) IrcController *ircController;

@end

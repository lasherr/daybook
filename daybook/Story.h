//
//  Story.h
//  daybook
//
//  Created by Development on 8/4/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Story : NSObject

@property NSString *channel;
@property NSString *name;
@property UIColor *color;
@property int messageCount;

-(void)log;

@end
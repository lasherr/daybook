//
//  IrcController.h
//  daybook
//
//  Created by Development on 7/19/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IrcClient.h"

@protocol IrcControllerDelegate <NSObject>

@optional
- (void)connectedToNetwork;
- (void)displayMessage:(NSString *)aMessageText from:(NSString *)aUserName silently:(BOOL)silent;
- (void)ircHasJoinedChannel;
@end


@interface IrcController : NSObject <IrcClientDelegate>

@property (nonatomic, strong) NSString *netWorck;

@property (nonatomic, weak) id<IrcControllerDelegate> delegate;

@property (nonatomic) BOOL isConnected;

- (NSMutableArray *)getChannels;
- (void) connectToNetWork;
- (void) connectToNetWorkServer: (NSString *)ip onPort: (int) port;
- (void) joinChannel:(NSString *)aChannel;
- (void) leaveCurrentChannel:(NSString *)aChannel;
- (void) sendMessage:message ToChanel:(NSString *) channel;
- (void) disconnect;

@end
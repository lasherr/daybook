//
//  StationPickerViewController.h
//  daybook
//
//  Created by Development on 7/20/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StationPickerDelegate <NSObject>

- (void) getDataForStation:(NSString *)station;

@end

@interface StationPickerViewController : UITableViewController

@property (nonatomic, weak) id<StationPickerDelegate> delegate;

@end

//
//  Utils.m
//  daybook
//
//  Created by Development on 8/4/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "Utils.h"


@implementation Utils

+ (NSString *)hexStringForColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    NSString *hexString=[NSString stringWithFormat:@"%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
    return hexString;
}

// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    if ( [hexString rangeOfString:@"#"].location == 0 ){
        [scanner setScanLocation:1]; // bypass '#' character
    }
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (UIColor *)randomMaterialColor{
    NSArray *colors = @[
                        [UIColor colorWithRed:0.96f green:0.26f blue:0.21f alpha:1.0f], //red
                        [UIColor colorWithRed:0.61f green:0.15f blue:0.69f alpha:1.0f], //purple
                        [UIColor colorWithRed:0.13f green:0.59f blue:0.95f alpha:1.0f], //blue
                        [UIColor colorWithRed:0.30f green:0.69f blue:0.31f alpha:1.0f], //green
                        [UIColor colorWithRed:1.0f green:0.92f blue:0.23f alpha:1.0f], //yellow
                        [UIColor colorWithRed:1.0f green:0.60f blue:0.0f alpha:1.0f], //orange
                        ];
    
    return colors[arc4random() % 6];
}

@end

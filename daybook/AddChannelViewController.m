//
//  AddChannelViewController.m
//  daybook
//
//  Created by Development on 7/21/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "AddChannelViewController.h"
#import "StationViewController.h"
#import "Story.h"

@interface AddChannelViewController(){
    NSArray *colorNames;
    NSArray *colors;
}
@end

@implementation AddChannelViewController


- (void) viewDidLoad{
    colorNames = @[@"red", @"purple", @"blue", @"green", @"yellow", @"orange"];
    colors = @[
                [UIColor colorWithRed:0.96f green:0.26f blue:0.21f alpha:1.0f], //red
                [UIColor colorWithRed:0.61f green:0.15f blue:0.69f alpha:1.0f], //purple
                [UIColor colorWithRed:0.13f green:0.59f blue:0.95f alpha:1.0f], //blue
                [UIColor colorWithRed:0.30f green:0.69f blue:0.31f alpha:1.0f], //green
                [UIColor colorWithRed:1.0f green:0.92f blue:0.23f alpha:1.0f], //yellow
                [UIColor colorWithRed:1.0f green:0.60f blue:0.0f alpha:1.0f], //orange
            ];
    
    [self.channelNameTextField becomeFirstResponder];
    [self.channelNameTextField setDelegate:self];
    
    [self.colorSegmentControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
}

- (IBAction)addNewChannel:(id)sender {
    [self askToConfirmChoice];
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)colorSelected:(id)sender {
    NSLog(@"%@", colorNames[self.colorSegmentControl.selectedSegmentIndex]);
    UISegmentedControl *betterSegmentedControl = self.colorSegmentControl;
    
    // Get number of segments
    NSUInteger numSegments = [betterSegmentedControl.subviews count];
    
    // Reset segment's color (non selected color)
    for( int i = 0; i < numSegments; i++ ) {
        // reset color
        [[betterSegmentedControl.subviews objectAtIndex:i] setTintColor:nil];
        [[betterSegmentedControl.subviews objectAtIndex:i] setTintColor:[UIColor blueColor]];
    }
    
    // Sort segments from left to right
    NSArray *sortedViews = [betterSegmentedControl.subviews sortedArrayUsingFunction:compareViewsByOrigin context:NULL];
    
    // Change color of selected segment
    NSInteger selectedIdx = betterSegmentedControl.selectedSegmentIndex;
    [[sortedViews objectAtIndex:selectedIdx] setTintColor:[colors objectAtIndex:selectedIdx]];
    
    // Remove all original segments from the control
    for (id view in betterSegmentedControl.subviews) {
        [view removeFromSuperview];
    }
    
    // Append sorted and colored segments to the control
    for (id view in sortedViews) {
        [betterSegmentedControl addSubview:view];
    }
}

NSInteger static compareViewsByOrigin(id sp1, id sp2, void *context)
{
    // UISegmentedControl segments use UISegment objects (private API). But we can safely cast them to UIView objects.
    float v1 = ((UIView *)sp1).frame.origin.x;
    float v2 = ((UIView *)sp2).frame.origin.x;
    if (v1 < v2)
        return NSOrderedAscending;
    else if (v1 > v2)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}

- (IBAction)textFieldFinished:(id)sender
{
    [self askToConfirmChoice];
}

- (void)askToConfirmChoice{
    NSString *message = [NSString stringWithFormat:@"Are you sure you want to add channel %@?", self.channelNameTextField.text];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Confirm channel name."
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {}];
    
    [alert addAction:cancelAction];
    
    UIAlertAction* confirm = [UIAlertAction actionWithTitle:@"Yes"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             NSString *userInput = self.channelNameTextField.text;
                                                             userInput = [userInput substringFromIndex:1];
                                                             
                                                             NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
                                                             [dateFormater setDateFormat:@"yyyy-MM-dd"];
                                                             NSString *date = [dateFormater stringFromDate:[NSDate date]];
                                                             
                                                             NSString *channelName = [NSString stringWithFormat:@"#%@__%@", date, userInput];
                                                             
                                                             int i = self.colorSegmentControl.selectedSegmentIndex;
                                                             
                                                             Story *story = [[Story alloc] init];
                                                             story.channel = channelName;
                                                             if (self.colorSegmentControl.selectedSegmentIndex == UISegmentedControlNoSegment) {
                                                                 story.color = nil;
                                                             } else {
                                                                 story.color = [colors objectAtIndex:i];
                                                             }
                                                             
                                                             [self.delegate addStoryObject:story];
//                                                             [self.delegate addChanelToList:channelName];
                                                             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 500), dispatch_get_main_queue(), ^{
                                                                 [self dismissViewControllerAnimated:YES completion:nil];
                                                             });
                                                         }];
    
    [alert addAction:confirm];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        
    }
}
@end

//
//  ircClient.m
//  daybook
//
//  Created by Development on 7/13/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "IrcClient.h"

@implementation IrcClient{
    NSString *IRC_COMMAND_JOIN;
    NSString *IRC_COMMAND_LEAVE;
    dispatch_queue_t messageQueue;
    
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    NSMutableArray *channels;

    BOOL signedIn;
    BOOL escapeIrcCommands;
}



- (id)init {
    self = [super init];
    if (self) {
        IRC_COMMAND_JOIN = @"JOIN";
        IRC_COMMAND_LEAVE = @"PART";
        messageQueue  = dispatch_queue_create("out.messages.irc.dgx", DISPATCH_QUEUE_SERIAL);
        channels = [[NSMutableArray alloc] init];
        signedIn = NO;
        escapeIrcCommands = YES;
    }
    return self;
}

- (void)startNetworkCommunicationOn:(NSString *)iP usingPort:(int)port;{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)iP, port, &readStream, &writeStream);
    
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    
    
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];
}

- (void)startNetworkCommunication:(NSString *)ip; {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)ip, 181, &readStream, &writeStream);
    
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    
    
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];
}

- (void)joinChannel:(NSString *)aChannel; {
    unichar firstChar = [[aChannel uppercaseString] characterAtIndex:0];
    if (firstChar != '#') {
        aChannel = [NSString stringWithFormat:@"%@%@",@"#",aChannel];
    }
    NSString *command = [NSString stringWithFormat:@"%@ %@", IRC_COMMAND_JOIN, aChannel];
    
    [self writeOut:command JumpQueue:NO];
}

- (void)leaveChannel:(NSString *)aChannel;{
    unichar firstChar = [[aChannel uppercaseString] characterAtIndex:0];
    if (firstChar != '#') {
        aChannel = [NSString stringWithFormat:@"%@%@",@"#",aChannel];
    }
    NSString *command = [NSString stringWithFormat:@"%@ %@", IRC_COMMAND_LEAVE, aChannel];
    
    [self writeOut:command JumpQueue:NO];
}
- (void)sendMessage:(NSString *)message ToChanel:(NSString *)channel;{
    //PRIVMSG <<#channel>> : <<message>>
    unichar firstChar = [[channel uppercaseString] characterAtIndex:0];
    if (firstChar != '#') {
        channel = [NSString stringWithFormat:@"%@%@",@"#",channel];
    }
    
    NSString *command = [NSString stringWithFormat:@"PRIVMSG %@ : %@", channel, message];
    
    [self writeOut:command JumpQueue:NO];
}

- (void)loginAndRegister {
    //write:
    //PASS daybook
    //USER  Test 0 *  : Ricky Lasher
    //NICK ios
    
    //TODO remove for real user info
    NSString *chars = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: 4];
    
    for (int i=0; i<4; i++) {
        [randomString appendFormat: @"%C", [chars characterAtIndex: arc4random_uniform([chars length])]];
    }
    
    NSString *nickCmd = [NSString stringWithFormat:@"NICK %@", randomString];

    [self writeOut:@"PASS daybook" JumpQueue:NO];
    [self writeOut:@"USER iosUserName 0 * : FirstName LastNight" JumpQueue:NO];
    [self writeOut:nickCmd JumpQueue:NO];
}

- (void)disconnectFromNetwork;{
    [self writeOut:@"QUIT" JumpQueue:NO];
}

- (void)writeOut:(NSString *)msg JumpQueue:(BOOL)jumpQueue{
    
    msg = [NSString stringWithFormat:@"%@\r\n", msg];
    if (escapeIrcCommands){
        msg = [msg stringByReplacingOccurrencesOfString:@"/" withString:@"&#47;"];
    }
    
    if (jumpQueue){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //not adding pongs to messafe queue so that i can answer pings quickly
            NSData *data = [[NSData alloc] initWithData:[msg dataUsingEncoding:NSUTF8StringEncoding]];
            [outputStream write:[data bytes] maxLength:[data length]];
        });
    } else {
        dispatch_async(messageQueue, ^{
            //tasks you have stored in the DISPATCH_QUEUE_SERIAL will wait and be executed one after the other in the order they were added, thanks to GCD (Grand Central Dispatch).
            NSData *data = [[NSData alloc] initWithData:[msg dataUsingEncoding:NSUTF8StringEncoding]];
            [outputStream write:[data bytes] maxLength:[data length]];
        });
    }
    
    NSLog(@"out:\r\n %@", msg);
}

- (NSString *)readIn{
    uint8_t buffer[1024];
    NSInteger len;
    
    while ([inputStream hasBytesAvailable]) {
        len = [inputStream read:buffer maxLength:sizeof(buffer)];
        if (len > 0) {
            
            NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
            
            if (nil != output) {
                NSLog(@"in:\r\n %@\r\n", output);
                return output;
            }
        }
    }
    
    return nil;
}

- (void)parseInput {
    //TODO parse each line of input sepereately
    
    NSString *input = [self readIn];
    
    if (input == nil){
        return;
    }
    
    NSString *PING = @"PING";
    NSString *EO_MOTD = @":End of /MOTD command.";
    NSString *EO_NAMES = @":End of /NAMES list.";
    NSString *PRIVMSG = @"PRIVMSG";
    
    NSRegularExpression *pongEchoRegex = [NSRegularExpression regularExpressionWithPattern:@"([^\\s]+)$"
                                                                                   options:NSRegularExpressionCaseInsensitive
                                                                                     error:nil];
    NSRegularExpression *privMsgRegex = [NSRegularExpression regularExpressionWithPattern:@":([\\w]+)!.+PRIVMSG (#[\\S]+) :(.+)$"
                                                                                  options:NSRegularExpressionCaseInsensitive
                                                                                    error:nil];
    
    NSArray *commands = [input componentsSeparatedByString:@"\r\n"];
    
    for (NSString *line in commands) {
        if ([line rangeOfString:PING].location == 0) {
            //is a ping
            
            //get the replay code
            NSArray *matches = [pongEchoRegex matchesInString:line
                                                      options:0
                                                        range:NSMakeRange(0, [line length])];
            
            NSString *echo = [line substringWithRange:[matches[0] range] ];//because we matched end of line we only have one element in matches
            NSString *pong = [NSString stringWithFormat:@"\r\nPONG %@\r\n", echo];
            
            [self writeOut:pong JumpQueue:YES];
            
        } else if ([line rangeOfString:EO_MOTD].location != NSNotFound){
            [self.delegate connectedToServer];
        } else if ([line rangeOfString:EO_NAMES].location != NSNotFound){
            [self.delegate ircHasJoinedChannel];
        } else if ([line rangeOfString:PRIVMSG].location != NSNotFound){
            NSArray *matches = [privMsgRegex matchesInString:line
                                                     options:0
                                                       range:NSMakeRange(0, [line length])];
            
            for (NSTextCheckingResult *match in matches) {
                NSString *sender = [line substringWithRange:[match rangeAtIndex:1]];//index 0 is the whole match...because apple that's why
                
                NSString *message = [line substringWithRange:[match rangeAtIndex:3]];
                if (escapeIrcCommands) {
                    message = [message stringByReplacingOccurrencesOfString:@"&#47;" withString:@"/"];
                }
                
                NSString *receiver = [line substringWithRange:[match rangeAtIndex:2]];
                
                NSLog(@"%@ says %@ to %@", sender, message, receiver);
                if (self.delegate){
                    [self.delegate recievedMessage:message from: sender];
                }
            }
        }
    }
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream opened");
            if (theStream == outputStream) {
                [self loginAndRegister];
                self.isConnected = YES;
            }
            break;
            
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStream) {
                [self parseInput];
            }
            
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            
            break;
            
        case NSStreamEventHasSpaceAvailable:
            
            //The NSStreamEventHasSpaceAvailable event is signalled when you can write to the stream without blocking.
            //Writing only in response to that event avoids that the current thread and possibly the user interface is blocked.
            
            //I didn't know this so I built in thread stuff.
            //we'll ignore this case just so the log doesn't fill with "Unknown event" when it's this case
            
            break;
            
        default:
            NSLog(@"Unknown event");
    }
    
}


@end

//
//  StationViewController.h
//  daybook
//
//  Created by Development on 7/20/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StationPickerViewController.h"
#import "IrcController.h"
#import "SWTableViewCell.h"
#import "AddChannelViewController.h"


@interface StationViewController : UITableViewController <StationPickerDelegate, SWTableViewCellDelegate, AddChanelViewControllerDelegate>


@end

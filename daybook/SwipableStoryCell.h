//
//  SwipableStoryCell.h
//  daybook
//
//  Created by Development on 8/4/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "SWTableViewCell.h"

@interface SwipableStoryCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIView *storyTagColorView;
@property (weak, nonatomic) IBOutlet UILabel *storyLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLable;

@end

//
//  NetworkViewController.m
//  daybook
//
//  Created by Development on 7/19/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "NetworkViewController.h"

#import "IrcController.h"
#import "MessageViewController.h"

@interface NetworkViewController ()

@property (nonatomic, strong) NSMutableArray *channels;

@property (nonatomic, strong) IrcController *ircController;

@end

@implementation NetworkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    
    self.ircController = [[IrcController alloc] init];
    self.channels = [_ircController getChannels];
    
    for (NSString *channel in _channels) {
        NSLog(@"%@", channel);
    }
    
    //[self.ircController connectToNetWork];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"buttonChatSeque"]){
        MessageViewController *destVc = segue.destinationViewController;
        
        destVc.channel = self.channels[0];
        destVc.ircController = self.ircController;
    }
}

@end

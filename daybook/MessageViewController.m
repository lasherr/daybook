//
//  MessageViewController.m
//  daybook
//
//  Created by Development on 7/14/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "SAMSoundEffect.h"
#import "MessageViewController.h"

#import "Message.h"
#import "MessageTableViewCell.h"
#import "ContinueMessageTableViewCell.h"
#import "MessageTextView.h"
#import "AppDelegate.h"

@interface MessageViewController ()

@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) UIWindow *pipWindow;

@property (nonatomic, strong) UIActivityIndicatorView *indicator;

@end

@implementation MessageViewController{
    BOOL loadingMessages;
    BOOL joiningChannel;
}

# pragma mark - init

- (instancetype)init
{
    self = [super initWithTableViewStyle:UITableViewStylePlain];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

+ (UITableViewStyle)tableViewStyleForCoder:(NSCoder *)decoder
{
    return UITableViewStylePlain;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self.tableView selector:@selector(reloadData) name:UIContentSizeCategoryDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textInputbarDidMove:) name:SLKTextInputbarDidMoveNotification object:nil];
    
    //listen for irc
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(joinedChannel) name:@"CDRircDidReceiveJoin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reJoinChannel)
                                                 name:@"ircDidConnectToServer"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRecieveMessageNotification:) name:@"ircDidReceiveMessage" object:nil];
    
    // Register a SLKTextView subclass, if you need any special appearance and/or behavior customisation.
    [self registerClassForTextView:[MessageTextView class]];
    
#if DEBUG_CUSTOM_TYPING_INDICATOR
    // Register a UIView subclass, conforming to SLKTypingIndicatorProtocol, to use a custom typing indicator view.
    [self registerClassForTypingIndicatorView:[TypingIndicatorView class]];
#endif
    [self configureDataSource];
}

- (void) configureDataSource{
    self.messages = [[NSMutableArray alloc] init];
}

#pragma mark - View lifecycle

- (void)  viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.ircController.messageDelegate = self;
    
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.center = self.view.center;
    self.indicator.hidesWhenStopped = YES;
    [self.indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    if ([self.indicator respondsToSelector:@selector(setColor:)]) {
        [self.indicator setColor:[UIColor grayColor]];
    }
    
        [self.view addSubview:self.indicator];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[MessageTableViewCell class] forCellReuseIdentifier:MessengerCellIdentifier];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(titleButtonSelected)
     forControlEvents:UIControlEventTouchUpInside];
    
    NSRange range = [self.channel rangeOfString:@"__"];
    NSString *displayName = [self.channel substringFromIndex:range.location + range.length];
    
    [button setTitle:displayName forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 170, 35);
    
    self.navigationItem.titleView = button;
}

- (void) viewDidAppear:(BOOL)animated{
    [self.indicator startAnimating];
    
    joiningChannel = YES;
    [self reJoinChannel];
    [self loadMessagesFromDb];
    while (loadingMessages && joiningChannel) {}
    [self.indicator stopAnimating];
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[(AppDelegate *)[[UIApplication sharedApplication] delegate] ircController] leaveCurrentChannel:self.channel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action Methods

- (void) titleButtonSelected{}

- (void)togglePIPWindow:(id)sender
{
    if (!_pipWindow) {
        [self showPIPWindow:sender];
    }
    else {
        [self hidePIPWindow:sender];
    }
}

- (void)showPIPWindow:(id)sender
{
    CGRect frame = CGRectMake(CGRectGetWidth(self.view.frame) - 60.0, 0.0, 50.0, 50.0);
    frame.origin.y = CGRectGetMinY(self.textInputbar.frame) - 60.0;
    
    _pipWindow = [[UIWindow alloc] initWithFrame:frame];
    _pipWindow.layer.cornerRadius = 10.0;
    _pipWindow.layer.masksToBounds = YES;
    _pipWindow.hidden = NO;
    _pipWindow.alpha = 0.0;
    
    [[UIApplication sharedApplication].keyWindow addSubview:_pipWindow];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         _pipWindow.alpha = 1.0;
                     }];
}

- (void)hidePIPWindow:(id)sender
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         _pipWindow.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         _pipWindow.hidden = YES;
                         _pipWindow = nil;
                     }];
}

- (void)textInputbarDidMove:(NSNotification *)note
{
    if (!_pipWindow) {
        return;
    }
    
    CGRect frame = self.pipWindow.frame;
    frame.origin.y = [note.userInfo[@"origin"] CGPointValue].y - 60.0;
    
    self.pipWindow.frame = frame;
}

- (void)didPressRightButton:(id)sender
{
    // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
    
    // This little trick validates any pending auto-correction or auto-spelling just after hitting the 'Send' button
    [self.textView refreshFirstResponder];
    
//    [self.ircController sendMessage:self.textView.text ToChanel:self.channel];
    [[(AppDelegate *)[[UIApplication sharedApplication] delegate] ircController] sendMessage:self.textView.text ToChanel:self.channel];
    
    if ([self.messages count] > 0){
        if ([((Message *)self.messages[0]).username isEqualToString:@"Me"]){
            //shoud be [User name] or something but we will know the username and we will know that here the
            // user is the author
            
            //add text to last
            Message *last = self.messages[0];
            last.text = [NSString stringWithFormat:@"%@\r\n%@", last.text, self.textView.text];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [super didPressRightButton:sender];
            
            return;
        }
    }
    
    Message *message = [Message new];
    message.username = @"Me";
    message.text = [self.textView.text copy];
    NSMutableAttributedString *atrString = [[NSMutableAttributedString alloc] initWithString:[self.textView.text copy]];
    message.attributedText = atrString;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
    UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    
    [self.tableView beginUpdates];
    [self.messages insertObject:message atIndex:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    
    // Fixes the cell from blinking (because of the transform, when using translucent cells)
    // See https://github.com/slackhq/SlackTextViewController/issues/94#issuecomment-69929927
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [super didPressRightButton:sender];
}

- (void)didLongPressCell:(UIGestureRecognizer *)gesture
{
    return;
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self messageCellForRowAtIndexPath:indexPath];
}

- (UITableViewCell *)messageCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //code commented out in this method was removed under the assumption that what I wrote on the plane works
    //no other code in this method was commented out before that
    //TODO test this!!!!
    
    Message *message = self.messages[indexPath.row];
    
    MessageTableViewCell *cell = (MessageTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];

    cell.titleLabel.text = message.username;
    cell.bodyLabel.text = message.text;
    
    cell.indexPath = indexPath;
    cell.usedForMessage = YES;
    
    // Cells must inherit the table view's transform
    // This is very important, since the main table view may be inverted
    cell.transform = self.tableView.transform;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tableView]) {
        
        Message *message = self.messages[indexPath.row];
        
        if (message.isContinuing){
            //TODO fix this to give smalller height
            NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
            paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
            paragraphStyle.alignment = NSTextAlignmentLeft;
            
            CGFloat pointSize = [MessageTableViewCell defaultFontSize];
            
            NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:pointSize],
                                         NSParagraphStyleAttributeName: paragraphStyle};
            
            CGFloat width = CGRectGetWidth(tableView.frame)-kMessageTableViewCellAvatarHeight;
            width -= 25.0;
            
            CGRect bodyBounds = [message.text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:NULL];
            
            if (message.text.length == 0) {
                return 0.0;
            }
            
            CGFloat height = CGRectGetHeight(bodyBounds);
            height += 8.0;
            
            if (height < kMessageTableViewCellMinimumHeight) {
                height = kMessageTableViewCellMinimumHeight;
            }
            
            return height;

        } else {
            //normal
            NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
            paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
            paragraphStyle.alignment = NSTextAlignmentLeft;
            
            CGFloat pointSize = [MessageTableViewCell defaultFontSize];
            
            NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:pointSize],
                                         NSParagraphStyleAttributeName: paragraphStyle};
            
            CGFloat width = CGRectGetWidth(tableView.frame)-kMessageTableViewCellAvatarHeight;
            width -= 25.0;
            
            CGRect titleBounds = [message.username boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:NULL];
            CGRect bodyBounds = [message.text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:NULL];
            
            if (message.text.length == 0) {
                return 0.0;
            }
            
            CGFloat height = CGRectGetHeight(titleBounds);
            height += CGRectGetHeight(bodyBounds);
            height += 40.0;
            
            if (height < kMessageTableViewCellMinimumHeight) {
                height = kMessageTableViewCellMinimumHeight;
            }
            
            return height;

        }
    } else {
        return kMessageTableViewCellMinimumHeight;
    }
}

#pragma mark - irc

-(void) didRecieveMessageNotification:(NSNotification *)note{
    if ([note.name isEqualToString:@"ircDidReceiveMessage"])
    {
        //NSDictionary *data = @{@"sender" : aUserName, @"message" : aMessageText, @"silent" : [NSNumber numberWithBool:silent]};
        NSDictionary* userInfo = note.userInfo;
        NSString *aMessage = (NSString *)userInfo[@"message"];
        NSString *aUserName = (NSString *)userInfo[@"sender"];
        
        [self displayMessage:aMessage from:aUserName silently:NO];
    }
}

-(void) reJoinChannel{
    [[(AppDelegate *)[[UIApplication sharedApplication] delegate] ircController] joinChannel:self.channel];
}

-(void) sendMessage: (NSString *) message{
    [[(AppDelegate *)[[UIApplication sharedApplication] delegate] ircController] sendMessage:message ToChanel:self.channel];
}

#pragma mark - irccontrollerdelegate

- (void)displayMessage:(NSString *)message from:(NSString *)name silently:(BOOL)silent;{
    if (![name isEqualToString:@"me"] && !silent){
        [SAMSoundEffect playSoundEffectNamed:@"ding"];
    }
    
    if ([self.messages count] > 0){
        Message *last = self.messages[0];
        if ([last.username isEqualToString:name]){
            //append last's text
            last.text = [NSString stringWithFormat:@"%@\r\n%@", last.text, message];
            
            //notify table that data changed
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            return;
        }
    }
    
    Message *messageObj = [Message new];
    messageObj.username = name;
    messageObj.text = message;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewRowAnimation rowAnimation = UITableViewRowAnimationBottom;
    UITableViewScrollPosition scrollPosition = UITableViewScrollPositionBottom;
    
    [self.tableView beginUpdates];
    [self.messages insertObject:messageObj atIndex:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    
    // Fixes the cell from blinking (because of the transform, when using translucent cells)
    // See https://github.com/slackhq/SlackTextViewController/issues/94#issuecomment-69929927
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void) joinedChannel{
    joiningChannel = NO;
}

#pragma mark - db methods

- (void) loadMessagesFromDb{
    loadingMessages = YES;
    NSString *sUrl = [NSString stringWithFormat:@"http://dgx.nobrolla.com/daybook/messages.php?channel=%@", self.channel];
    sUrl = [sUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@", sUrl);
    NSURL *url = [NSURL URLWithString:sUrl];
    NSURLSession *urlSession = [NSURLSession sharedSession];

    NSURLSessionDataTask *dataTask = [urlSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSString *log = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@", log);
            NSLog(@"Status code: %li", (long)((NSHTTPURLResponse *)response).statusCode);
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            for (NSDictionary *jsonObj in json) {
                NSString *message = [jsonObj objectForKey:@"message"];
                NSString *sender = [jsonObj objectForKey:@"sender"];
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self displayMessage:message from:sender silently:YES];
                });
                
                continue;
            }
            NSLog(@"%lu", (unsigned long)[self.messages count]);
            
            loadingMessages = false;
        } else {
            NSLog(@"%@", error);
        }
    }];
    
    [dataTask resume];
}

@end

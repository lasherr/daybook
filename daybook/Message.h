//
//  Message.h
//  daybook
//
//  Created by Development on 7/14/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Message : NSObject

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSMutableAttributedString *attributedText;

@property (nonatomic) BOOL isContinuing;

@end

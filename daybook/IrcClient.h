//
//  ircClient.h
//  daybook
//
//  Created by Development on 7/13/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IrcClientDelegate <NSObject>
@required

- (void)recievedMessage:(NSString *)message from:(NSString *)name;
- (void)connectedToServer;
- (void)ircHasJoinedChannel;

@end

@interface IrcClient : NSObject <NSStreamDelegate>{
    
    // Delegate to respond back
    id <IrcClientDelegate> _delegate;
}

@property (nonatomic,strong) id delegate;
@property (nonatomic)     BOOL isConnected;

- (void)startNetworkCommunication:(NSString *)iP;
- (void)startNetworkCommunicationOn:(NSString *)iP usingPort:(int)port;
- (void)joinChannel:(NSString *)aChannel;
- (void)leaveChannel:(NSString *)aChannel;
- (void)sendMessage:(NSString *)message ToChanel:(NSString *)channel;
- (void)disconnectFromNetwork;

@end

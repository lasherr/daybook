//
//  Story.m
//  daybook
//
//  Created by Development on 8/4/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "Story.h"
#import "Utils.h"

@implementation Story

-(void)log;
{
    NSString *colorCode = @"";
    if(self.color){
        colorCode = [Utils hexStringForColor:self.color];
    }
    NSLog(@"story =>   name: %@   tag:%@", self.name, colorCode);
}
@end

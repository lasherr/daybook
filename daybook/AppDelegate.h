//
//  AppDelegate.h
//  daybook
//
//  Created by Development on 7/12/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IrcController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, IrcControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) IrcController *ircController;

@end
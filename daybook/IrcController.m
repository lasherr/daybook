//
//  IrcController.m
//  daybook
//
//  Created by Development on 7/19/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "IrcController.h"

@implementation IrcController{
    IrcClient *client;
}

- (id) init{
    self = [super init];
    if (self){
        client = [[IrcClient alloc] init];
        client.delegate = self;
        self.isConnected = NO;
    }
    return self;
}

- (NSMutableArray *)getChannels{
    return [NSMutableArray arrayWithObjects: @"#iosDev", @"#exp", nil];
}

- (void) connectToNetWorkServer: (NSString *)ip onPort: (int) port;{
    if (!self.isConnected || ![self.netWorck isEqualToString:ip]){
        self.netWorck = ip;
        //return [client startNetworkCommunication:ip];
        [client startNetworkCommunicationOn:ip usingPort:port];
    }
}

- (void) connectToNetWork;{
    if (!self.isConnected || ![self.netWorck isEqualToString:@"209.41.186.69"]){
        self.netWorck = @"209.41.186.69";
        return [client startNetworkCommunication:@"209.41.186.69"];
    }
}

- (void) joinChannel:(NSString *)aChannel{
    return [client joinChannel:aChannel];
}

- (void)leaveCurrentChannel:(NSString *)aChannel{
    return [client leaveChannel:aChannel];
}

- (void) disconnect;{
    self.isConnected = NO;
    [client disconnectFromNetwork];
}

- (void) sendMessage:message ToChanel:(NSString *) channel;{
    return [client sendMessage:message ToChanel:channel];
}

#pragma mark -IrcClientDelegate

- (void)connectedToServer;{
    self.isConnected = YES;
    if (self.delegate){
        if([self.delegate respondsToSelector:@selector(connectedToNetwork)]){
            [self.delegate connectedToNetwork];
        }
    }
}

- (void)recievedMessage:(NSString *)message from:(NSString *)name;{
    if([self.delegate respondsToSelector:@selector(displayMessage:from:silently:)]){
        [self.delegate displayMessage:message from:name silently: NO];
    }
}

- (void)ircHasJoinedChannel;{
    if([self.delegate respondsToSelector:@selector(ircHasJoinedChannel)]){
        [self.delegate ircHasJoinedChannel];
    }
}

@end

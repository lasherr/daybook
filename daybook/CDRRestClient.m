//
//  CDRRestClient.m
//  daybook
//
//  Created by Development on 7/26/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "CDRRestClient.h"

@implementation CDRRestClient

- (id) init {
    self = [super init];
    if (self){
        
    }
    return self;
}

- (void) getNetworkData{
    NSURLSession *urlSession = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:@"http://dgx.nobrolla.com/daybook/networks.php"];
    NSURLSessionDataTask *dataTask = [urlSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSArray *chanels = [[json valueForKey:@"chanels"] objectAtIndex:0];
        
        for (NSString *channel in chanels) {
            NSLog(@"%@", channel);
        }
        
        NSLog(@"%@", json);
    }];
    [dataTask resume];
}

@end

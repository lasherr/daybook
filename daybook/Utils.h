//
//  Utils.h
//  daybook
//
//  Created by Development on 8/4/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject

+ (NSString *)hexStringForColor:(UIColor *)color;
+ (UIColor *)randomMaterialColor;
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end

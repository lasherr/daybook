//
//  AppDelegate.m
//  daybook
//
//  Created by Development on 7/12/16.
//  Copyright © 2016 CDR. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "MessageViewController.h"
#import "StationViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    
    self.ircController = [[IrcController alloc] init];
    self.ircController.delegate = self;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self.ircController disconnect];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (!self.ircController.isConnected){
        [self.ircController connectToNetWork];
    }
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"ApplicationWillEnterForground" object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


#pragma mark - IrcControllerDelegate

- (void)connectedToNetwork;{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ircDidConnectToServer" object:nil];
}

- (void)displayMessage:(NSString *)aMessageText from:(NSString *)aUserName silently:(BOOL)silent;{
    NSDictionary *data = @{@"sender" : aUserName, @"message" : aMessageText, @"silent" : [NSNumber numberWithBool:silent]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ircDidReceiveMessage" object:self userInfo:data ];
}

- (void)ircHasJoinedChannel;{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CDRircDidReceiveJoin" object:nil ];
}

//((UINavigationController*)appDelegate.window.rootViewController).visibleViewController;

@end

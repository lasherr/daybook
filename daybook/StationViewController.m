//
//  StationViewController.m
//  daybook
//
//  Created by Development on 7/20/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "StationViewController.h"

#import "CDRRestClient.h"
#import "MessageViewController.h"
#import "IrcController.h"
#import "AppDelegate.h"
#import "Story.h"
#import "SwipableStoryCell.h"
#import "Utils.h"

@interface StationViewController ()

@property (nonatomic, strong) NSString *station;
//@property (nonatomic, strong) IrcController *ircController;
@property (nonatomic, strong) AddChannelViewController *addChannelPopup;
@property (nonatomic, strong) NSMutableArray *displayChannelNames;
@property (nonatomic, strong) NSMutableArray *storyObjects;
@property (nonatomic, strong) NSString *ip;
@property (nonatomic) int port;

@property (nonatomic, strong) UIDatePicker *myPicker;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;

@property (nonatomic, strong) NSURLSessionDataTask *dataTask;

@end

@implementation StationViewController{
    NSMutableArray *ircChannelNames;
    int selectedStoryIndex;
}

#pragma mark - init

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void) commonInit{
    //TODO change
    self.station = @"kdev";
    
    [self listenForIrcNotifications];
    
    ircChannelNames = [[NSMutableArray alloc] init];
    self.storyObjects = [[NSMutableArray alloc] init];
    self.displayChannelNames = [[NSMutableArray alloc] init];
}

#pragma mark - lifecycle

- (void) viewDidLoad;{
    [super viewDidLoad];
    
    //custom swipe table cells
    [self.tableView registerNib:[UINib nibWithNibName:@"SwipableStoryCell" bundle:nil] forCellReuseIdentifier:@"StoryTableCell"];
    
    //stories table view
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    [self.tableView registerClass:[SWTableViewCell class] forCellReuseIdentifier:@"StoryTableCell"];
    
    //activity indicator
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.center = self.view.center;
    self.indicator.hidesWhenStopped = YES;
    [self.indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    if ([self.indicator respondsToSelector:@selector(setColor:)]) {
        [self.indicator setColor:[UIColor grayColor]];
    }
    
    [self.indicator startAnimating];
    [self.view addSubview:self.indicator];
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithRed:0.05f green:0.48f blue:1.0f alpha:1.0f];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(loadChannelsFromDb)
                  forControlEvents:UIControlEventValueChanged];
    
    
    //header
    
        //sizes
    CGRect bounds = [self.tableView bounds];
    int w = bounds.size.width;
    int pickerHeight = 60;
    int buttonHeight =  20;
    int margin = 5;
    int headerHeight = pickerHeight + buttonHeight + margin;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, w, headerHeight)];
    
        //picker
    self.myPicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, w, pickerHeight)];
    [self.myPicker addTarget:self
                 action:@selector(pickerChanged:)
       forControlEvents:UIControlEventValueChanged];
    [self.myPicker setDatePickerMode:UIDatePickerModeDate];
    [headerView addSubview:self.myPicker];
    
        //sort control
    NSArray *itemArray = [NSArray arrayWithObjects: @"hot", @"new", @"following", nil];
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    segmentedControl.frame = CGRectMake(4, pickerHeight, w-8, buttonHeight);
    [segmentedControl addTarget:self action:@selector(MySegmentControlAction:) forControlEvents: UIControlEventValueChanged];
    segmentedControl.selectedSegmentIndex = 0;
    [headerView addSubview:segmentedControl];
    
        //margin
    UIView *marginView = [[UIView alloc] initWithFrame:CGRectMake(0, pickerHeight + buttonHeight, w, margin)];
    [headerView addSubview:marginView];
    
        //add header
    self.tableView.tableHeaderView = headerView;
    
    //'+' button in nav bar
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addStory:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
//    self.ircController = [[IrcController alloc] init];
//    self.ircController.networkDelegate = self;
    
    [self loadNetworkInfo];
    [self setTitleButton];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIImageView* imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, 100, 30)];
    imgView.image = [UIImage imageNamed:@"DAYBOOK_HORIZONTAL"];
    imgView.tag = 1234;
    [self.navigationController.navigationBar addSubview:imgView];

}

#pragma mark - action methods

- (void)MySegmentControlAction:(UISegmentedControl *)segment {
//    data = [[data reverseObjectEnumerator] allObjects];
//    [self.tableView reloadData];
}

-(void) pickerChanged:(NSObject *)obj{
    NSLog(@"%@", [obj class]);
    [self loadChannelsFromDb];
    
}

-(void) addStory:(NSObject *)sender{
        
    // grab the view controller we want to show
    UIStoryboard *storyboard = [self storyboard];
    AddChannelViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"4321"];
    controller.delegate = self;
    
    // present the controller
    // on iPad, this will be a Popover
    // on iPhone, this will be an action sheet
    controller.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:controller animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.sourceRect = CGRectMake(0, 0, 100, 100);
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.rightBarButtonItem;
//    popController.delegate = self;
    
    return ;
}

# pragma mark - navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if (view.tag == 1234) {
            [view removeFromSuperview];
        }
    }
    
    if ([segue.identifier isEqualToString:@"ShowStationPickerSegue"]){
        StationPickerViewController *neVc = segue.destinationViewController;
        neVc.delegate = self;
    } else if ([segue.identifier isEqualToString:@"MessagesSegue"]){
        MessageViewController *newVc = segue.destinationViewController;
        NSIndexPath *selectedIndex = [self.tableView indexPathForSelectedRow];
        
        int index = (int)selectedIndex.row;
        newVc.network = self.station;
        newVc.channel = ircChannelNames[index];
        //newVc.ircController = self.ircController;
    }
}

-(void)segueToStationPicker:(NSObject *)sender{
    [self performSegueWithIdentifier:@"ShowStationPickerSegue" sender:self];
}

#pragma mark - networking

-(void) loadNetworkInfo{
    NSURLSession *urlSession = [NSURLSession sharedSession];
    
    NSURL *url = [NSURL URLWithString:@"http://dgx.nobrolla.com/daybook/networks.php"];
    NSURLSessionDataTask *dataTask = [urlSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@", json);
        self.ip = [[json valueForKey:@"ip"] objectAtIndex:0];
        self.port = [[[json valueForKey:@"port"] objectAtIndex:0] intValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[(AppDelegate *)[[UIApplication sharedApplication] delegate] ircController] connectToNetWorkServer:self.ip onPort:self.port];
            //this is a lot of casting and singleton action but its essentially [sharedIrcController connectToServer];
        });
    }];
    [dataTask resume];
}

- (void)loadChannelsFromDb{
    if (self.dataTask){
        [self.dataTask cancel];
    }
    
    if (![self.indicator isAnimating]) {
        [self.indicator startAnimating];
    }
    
    NSURLSession *urlSession = [NSURLSession sharedSession];
    
    NSDate *date = [self.myPicker date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    NSString *sUrl = [NSString stringWithFormat:@"http://dgx.nobrolla.com/daybook/channels.php?letters=%@&date=%@", self.station, stringFromDate];
    NSURL *url = [NSURL URLWithString:sUrl];
    self.dataTask = [urlSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            
            ircChannelNames = [[NSMutableArray alloc] init];
            self.displayChannelNames = [[NSMutableArray alloc] init];
            self.storyObjects = [[NSMutableArray alloc] init];
            
            NSString *log = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@", log);
            NSLog(@"Status code: %li", (long)((NSHTTPURLResponse *)response).statusCode);
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            
            for (NSDictionary *jsonObj in json) {
                
                NSString *channel = [jsonObj objectForKey:@"name"];
                NSString *hexColor = [jsonObj objectForKey:@"tag_color"];
                NSString *countString = [jsonObj objectForKey:@"message_count"];
                
                
                NSRange range = [channel rangeOfString:@"__"];
                NSString *displayName = [channel substringFromIndex:range.location + range.length];
                
                displayName = [NSString stringWithFormat:@"#%@", displayName];
                
                Story *s = [[Story alloc] init];
                s.channel = channel;
                s.name = displayName;
                if (hexColor != (NSString *)[NSNull null]){
                    if (![hexColor isEqualToString:@""]) {
                        s.color = [Utils colorFromHexString:hexColor];
                    }
                }
                s.messageCount = 0;
                if (countString != (NSString *)[NSNull null]){
                    if (![countString isEqualToString:@""]) {
                        s.messageCount = [countString intValue];
                    }
                }
                
                [self.storyObjects addObject:s];
                [self.displayChannelNames addObject:displayName];
                [ircChannelNames addObject:channel];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.indicator stopAnimating];
                [self.tableView reloadData];
            });
        } else {
            NSLog(@"%@", error);
        }
        
        [self.refreshControl endRefreshing];
    }];
    
    [self.dataTask resume];
    
}

# pragma mark - helpers

- (NSArray *)rightButtons:(BOOL) isSubscribing
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    if (isSubscribing){
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0.22f green:0.52f blue:0.24f alpha:1.0]
                                                    title:@"Unfollow"];
    } else {
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                    title:@"Follow"];
    }
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:.07f green:.35f blue:.57f alpha:1.0]
                                                title:@"Copy To"];
    return rightUtilityButtons;
}

-(void) setTitleButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(segueToStationPicker:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:_station forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 170, 35);
    
    self.navigationItem.titleView = button;

}

#pragma mark - datasource delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ircChannelNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Story *currentStory = self.storyObjects[indexPath.row];
    [currentStory log];
    
//    NSString *labelText = self.displayChannelNames[indexPath.row];
    
    SwipableStoryCell *cell = (SwipableStoryCell *)[self.tableView dequeueReusableCellWithIdentifier:@"StoryTableCell"];
    
    if (cell == nil){
        cell = [[SwipableStoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"StoryTableCell"];
    }
    
    cell.storyLabel.text = currentStory.name;
    if (currentStory.color){
        [cell.storyTagColorView setBackgroundColor:currentStory.color];
    } else {
        [cell.storyTagColorView setBackgroundColor:[UIColor whiteColor]];
    }
    if (currentStory.messageCount > 1){
        cell.countLable.text = [NSString stringWithFormat:@"%d", currentStory.messageCount];//currentStory.messageCount;
    } else {
        cell.countLable.text = @"";
    }
    cell.rightUtilityButtons = [self rightButtons:NO];
    cell.delegate = self;
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedStoryIndex = (int)indexPath.row;
    NSIndexPath *selectedIndex = [self.tableView indexPathForSelectedRow];
    
    int index = (int)selectedIndex.row;
    if (index < [ircChannelNames count]){
        [self performSegueWithIdentifier:@"MessagesSegue" sender:self];
    }
}

#pragma mark - stationpicker delegate

- (void) getDataForStation:(NSString *)station;{
    if ([station isEqualToString:self.station]){
        return;
    }
    self.station = station;
    [self loadNetworkInfo];
    
    [self.tableView reloadData];
    [self setTitleButton];
}

#pragma mark - irc notifications

- (void) listenForIrcNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(connectedToNetwork)
                                                 name:@"ircDidConnectToServer"
                                               object:nil];
}

- (void)connectedToNetwork;{
    NSLog(@"%@" , @"connected to server");
    //load channels
    [self loadChannelsFromDb];
}

#pragma mark - SWTableViewCellDelegate

// click event on right utility button
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index;{
    if (index == 0){
        //follow/unfollow
        UIButton *selectedButton = cell.rightUtilityButtons[0];
        
        NSLog(@"%@", selectedButton.titleLabel.text);
        
        NSArray *rightUtilityButtons = [self rightButtons:[selectedButton.titleLabel.text isEqualToString:@"Follow"]];
        
        cell.rightUtilityButtons = rightUtilityButtons;
    } else if (index == 1){
        //copy to
    }
}

// prevent multiple cells from showing utilty buttons simultaneously
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell;{
    return YES;
}

#pragma mark - AddChanelViewControllerDelegate

-(void) addStoryObject: (Story *)aStoryObj;
{
    NSString *channel = aStoryObj.channel;
    NSRange range = [channel rangeOfString:@"__"];
    NSString *displayName = [channel substringFromIndex:range.location + range.length];
    
    displayName = [NSString stringWithFormat:@"#%@", displayName];
    aStoryObj.name = displayName;
    NSString *hexColor = @"";
    if (aStoryObj.color != nil){
        hexColor = [Utils hexStringForColor:aStoryObj.color];
    }
    
    //post request w/ url
    NSURL *url = [NSURL URLWithString:@"http://dgx.nobrolla.com/daybook/channels.php"];
    NSMutableURLRequest *mRequest = [NSMutableURLRequest requestWithURL:url];
    mRequest.HTTPMethod = @"POST";
    
    //post variables
    NSString * params = [NSString stringWithFormat:@"name=%@&network=%@&color=%@", channel, self.station, hexColor];
    [mRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *request = [mRequest copy];
    
    // Create a task.
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error)
                                  {
                                      if (!error){
                                          NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                          NSLog(@"%@", strData);
                                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                          for (NSString *sting in json) {
                                              NSLog(@"%@", sting);
                                          }
                                      }
                                      else
                                      {
                                          NSLog(@"Error: %@", error.localizedDescription);
                                      }
                                  }];
    
    // Start the task.
    [task resume];
    
    [self.storyObjects addObject:aStoryObj];
    
//    [self.displayChannelNames addObject:displayName];
//    [ircChannelNames addObject:channel];
    [self.tableView reloadData];

}
/*
-(void) addChanelToList:(NSString *)channel;{
    
    NSRange range = [channel rangeOfString:@"__"];
    NSString *displayName = [channel substringFromIndex:range.location + range.length];
    
    displayName = [NSString stringWithFormat:@"#%@", displayName];
    
    //post request w/ url
    NSURL *url = [NSURL URLWithString:@"http://dgx.nobrolla.com/daybook/channels.php"];
    NSMutableURLRequest *mRequest = [NSMutableURLRequest requestWithURL:url];
    mRequest.HTTPMethod = @"POST";
    
    //post variables
    NSString * params = [NSString stringWithFormat:@"name=%@&network=%@", channel, self.station];
    [mRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *request = [mRequest copy];
    
    // Create a task.
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error)
                                  {
                                      if (!error)
                                      {
                                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                          for (NSString *sting in json) {
                                              NSLog(@"%@", sting);
                                          }
                                      }
                                      else
                                      {
                                          NSLog(@"Error: %@", error.localizedDescription);
                                      }
                                  }];
    
    // Start the task.
    [task resume];

    
    [self.displayChannelNames addObject:displayName];
    [ircChannelNames addObject:channel];
    [self.tableView reloadData];
}
*/
#pragma mark - TableViewController Stuff
//this is just to get rid of the stupid apple tavle devider margin

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end

//
//  AddChannelViewController.h
//  daybook
//
//  Created by Development on 7/21/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Story;

@protocol AddChanelViewControllerDelegate <NSObject>

-(void) addStoryObject: (Story *)aStoryObj;
//-(void) addChanelToList: (NSString *)channel;

@end

@interface AddChannelViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *colorSegmentControl;
@property (weak, nonatomic) IBOutlet UITextField *channelNameTextField;
@property (weak, nonatomic) id<AddChanelViewControllerDelegate> delegate;

- (IBAction)addNewChannel:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)colorSelected:(id)sender;

@end

//
//  StationPickerViewController.m
//  daybook
//
//  Created by Development on 7/20/16.
//  Copyright © 2016 CDR. All rights reserved.
//

#import "StationPickerViewController.h"

@implementation StationPickerViewController{
    NSArray *data;
}

-(void) viewDidLoad{
    [super viewDidLoad];

    data = [NSArray arrayWithObjects:@"kdvr", nil];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"StationTableCell"];
    
}
#pragma mark datasource delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *labelText = data[indexPath.row];
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"StationTableCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"StationTableCell"];
    }
    
    cell.textLabel.text = labelText;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate getDataForStation:data[indexPath.row]];
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
